//
//  AppDelegate.h
//  snapPay
//
//  Created by ANZ MAC PRO 3 on 17/11/14.
//  Copyright (c) 2014 ANZ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

